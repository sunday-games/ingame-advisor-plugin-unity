﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

namespace IngameAdvisor
{
    public class Tests : MonoBehaviour
    {
        static Tests _instance;
        public static Tests instance { get { if (_instance == null) _instance = FindObjectOfType(typeof(Tests)) as Tests; return _instance; } }
        public bool showAll = false;
        public float delayBetweenRequests = 0f;
        public Text statusText;
        Text outputText;
        List<Test> tests = new List<Test>();
        float testsCompleted;

        public void Run(Project project, Text outputText)
        {
            if (string.IsNullOrEmpty(project.spreadsheetId) || string.IsNullOrEmpty(project.gidId))
            {
                Debug.LogError("Ingame Advisor - Tests - spreadsheetId and gidId cant be empty");
                return;
            }

            this.outputText = outputText;

            StartCoroutine(LoadCoroutine(project));
        }

        IEnumerator LoadCoroutine(Project project)
        {
            //var www = new WWW(string.Format("https://docs.google.com/spreadsheets/d/{0}/gviz/tq?tqx=out:csv&gid={1}",
            //    project.spreadsheetId, project.gidId));
            var www = new WWW(string.Format("https://docs.google.com/spreadsheets/d/{0}/export?format=csv&gid={1}",
                project.spreadsheetId, project.gidId));
            yield return www;

            if (!string.IsNullOrEmpty(www.error))
            {
                Debug.LogErrorFormat("Ingame Advisor - Tests - Download Error: {0} {1}", www.error, www.text);
            }
            else
            {
                Debug.Log("Ingame Advisor - Tests - Download Success");

                var reader = new SG.ByteReader(www.bytes);

                tests.Clear();
                testsCompleted = 0;

                var row = reader.ReadCSV();

                if (project.skipFirstRaw) row = reader.ReadCSV();

                while (row != null)
                {
                    if (row.size > 1)
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(row[0]) && !string.IsNullOrEmpty(row[1]))
                            {
                                string[] questions = row[0].Split(new char[] { '\n' });
                                foreach (var question in questions)
                                    if (!string.IsNullOrEmpty(question))
                                        tests.Add(new Test(project.language, question, row[1]));
                            }
                        }
                        catch (Exception e)
                        {
                            Debug.LogError("Ingame Advisor - Tests - Unable to add '" + row[0] + "' to the Localization dictionary: " + e.Message);
                        }
                    }
                    row = reader.ReadCSV();
                }

                if (delayBetweenRequests > 0f)
                    StartCoroutine(TestCoroutine());
                else
                    foreach (var test in tests)
                        Advisor.instance.Ask(new Request(test.text), test.language, OnGetReply, test);
            }

            www.Dispose();
        }

        IEnumerator TestCoroutine()
        {
            foreach (var test in tests)
            {
                Advisor.instance.Ask(new Request(test.text, SystemInfo.deviceUniqueIdentifier), test.language, OnGetReply, test);
                yield return new WaitForSeconds(delayBetweenRequests);
            }
        }

        void OnGetReply(Status status, Response response, Request request, Test test)
        {
            if (statusText) statusText.text = Mathf.CeilToInt((++testsCompleted / tests.Count) * 100) + "%";

            if (status != Status.Success) return;

            if (response.input != test.input)
                outputText.text = string.Format("<color=red>{0} >> {1} ({2} expected)</color>\n", test.text, response.input, test.input) + outputText.text;
            else if (showAll)
                outputText.text = outputText.text + string.Format("\n{0} >> {1}", test.text, test.input);
        }

        public void RunPerformance(Project project, Text outputText)
        {
            this.outputText = outputText;

            StartCoroutine(PerformanceCoroutine(project));
        }

        IEnumerator PerformanceCoroutine(Project project)
        {
            startTime = DateTime.Now;
            while (true)
            {
                for (int i = 0; i < 10; i++)
                    Advisor.instance.Ask(new Request("Нагрузочное тестирование " + UnityEngine.Random.Range(100000, 999999)), project.language, OnGetPerformanceReply);

                yield return null;
            }
        }

        DateTime startTime;
        int errors;
        int requests;
        void OnGetPerformanceReply(Status status, Response response, Request request, Test test)
        {
            requests++;

            if (status != Status.Success) errors++;

            outputText.text = string.Format("Errors {0}%\nRPS {1}", (float)errors / requests, (int)((float)requests / (DateTime.Now - startTime).TotalSeconds));
        }
    }

    public class Test
    {
        public SystemLanguage language = SystemLanguage.English;
        public string text;
        public string input;

        public Test(SystemLanguage language, string text, string input)
        {
            this.language = language;
            this.text = text;
            this.input = input;
        }
    }
}