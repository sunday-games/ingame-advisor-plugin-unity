﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Version 1.1

namespace IngameAdvisor
{
    public class Advisor : MonoBehaviour
    {
        static Advisor _instance;
        public static Advisor instance { get { if (_instance == null) _instance = FindObjectOfType(typeof(Advisor)) as Advisor; return _instance; } }

        public List<Project> projects;

        [Space(10)]
        public float timeout = 10f;

        [Space(10)]
        public bool logging = false;

        string GetProjectId(SystemLanguage language)
        {
            foreach (var project in projects)
                if (project.language == language) return project.id;
            return null;
        }

        public void Ask(Request request, SystemLanguage language, Action<Status, Response, Request, Test> callback, Test test = null)
        {
            if (request == null || callback == null)
            {
                Debug.LogError("Ingame Advisor - Request and Callback cant be null");
                return;
            }

            if (string.IsNullOrEmpty(GetProjectId(language)))
            {
                Debug.LogErrorFormat("Ingame Advisor - Language {0} is unavailable", language);
                return;
            }

            if (logging) Debug.Log("Ask: " + request.text);

            Download.Run(GetProjectId(language), request, callback, test);
        }
    }

    [Serializable]
    public class Project
    {
        public SystemLanguage language = SystemLanguage.English;
        public string id;
        public string spreadsheetId;
        public string gidId = "0";
        public bool skipFirstRaw = true;

        public Project(SystemLanguage language, string id)
        {
            this.language = language;
            this.id = id;
        }
    }
}