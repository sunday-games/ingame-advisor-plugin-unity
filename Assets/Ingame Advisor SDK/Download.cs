﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IngameAdvisor
{
    public enum Status { Success, Error, Timeout, NoConnection, TestFailed };

    public class Download : MonoBehaviour
    {
        static Dictionary<string, string> headers = new Dictionary<string, string>() { { "Content-Type", "application/json" } };
        //static string server = "https://zenbot.org/api/";
        static string server = "https://zenbot-dev.just-ai.com/chatadapter/chatapi/";

        public static void Run(string projectId, Request request, Action<Status, Response, Request, Test> callback, Test test = null)
        {
            Advisor.instance.gameObject.AddComponent<Download>()
                .Setup(projectId, request, callback, test);
        }

        void Setup(string projectId, Request request, Action<Status, Response, Request, Test> callback, Test test)
        {
            var www = new WWW(server + projectId, System.Text.Encoding.UTF8.GetBytes(request.ToJson()), headers);

            StartCoroutine(DownloadCoroutine(request, www, callback, test));
            StartCoroutine(TimeoutCoroutine(request, www, callback, test));
        }

        IEnumerator DownloadCoroutine(Request request, WWW www, Action<Status, Response, Request, Test> callback, Test test)
        {
            DateTime startTime = DateTime.Now;
            yield return www;
            var time = string.Format("{0:0.0}", (DateTime.Now - startTime).TotalSeconds);

            StopAllCoroutines();

            if (!string.IsNullOrEmpty(www.error))
            {
                if (www.error.Contains("Could not resolve host"))
                {
                    Debug.LogFormat("Ingame Advisor - NoConnection");
                    callback(Status.NoConnection, null, request, test);
                }
                else
                {
                    Debug.LogErrorFormat("Ingame Advisor - Error ({0}): {1}\n{2}", time, www.error, request.text);
                    callback(Status.Error, null, request, test);
                }
            }
            else
            {
                if (Advisor.instance.logging) Debug.LogFormat("Ingame Advisor - Success ({0}). Response: {1}", time, www.text);

                callback(Status.Success, Response.FromJson(www.text), request, test);
            }

            www.Dispose();

            Destroy(this);
        }
        IEnumerator TimeoutCoroutine(Request request, WWW www, Action<Status, Response, Request, Test> callback, Test test)
        {
            float startTime = Time.time;
            while (Time.time - startTime < Advisor.instance.timeout) yield return null;

            StopAllCoroutines();

            Debug.LogErrorFormat("Ingame Advisor - Timeout\n{0}", request.text);

            callback(Status.Timeout, null, request, test);

            www.Dispose();

            Destroy(this);
        }
    }

    [Serializable]
    public class Var
    {
        public string name;
        public string value;
        public string scope;

        public Var(string name, object value, string scope = "user")
        {
            this.name = name;
            this.value = value.ToString();
            this.scope = scope;
        }
    }

    [Serializable]
    public class Reply
    {
        public static Reply FromJson(string json)
        {
            return JsonUtility.FromJson<Reply>(json);
        }

        public string name;
        public string answer;

        public string url;

        public string email;
        public string subject;
        public string body;

        public Reply(string name)
        {
            this.name = name;
        }
    }

    public class Response
    {
        public static Response FromJson(string text)
        {
            // return JsonUtility.FromJson<Response>(json);

            var json = Json.Deserialize(text) as Dictionary<string, object>;
            var data = json["data"] as Dictionary<string, object>;
            var replies = data["replies"] as List<object>;
            /* {
                "clientId":"8104d360d8e5ed26757c3b727671fc22ff976d76",
                "questionId":"5d537cc2-ec05-46e8-a7ec-6f02ccb02a5d",
                "data":{
                    "answer":"Привет! Меня зовут Няша. Меня так назвали, потому что нужно было что-то милое и это первое, что пришло в голову моему создателю. Мне мое имя не нравится и я хотел бы быть Ягуаром. Ну, или Братиславом, на худой конец... Но довольно обо мне. Что бы ты хотел узнать о работе в Creative Mobile, хозяин?",
                    "replies":[
                        {
                            "type":"text",
                            "text":"Привет! Меня зовут Няша. Меня так назвали, потому что нужно было что-то милое и это первое, что пришло в голову моему создателю. Мне мое имя не нравится и я хотел бы быть Ягуаром. Ну, или Братиславом, на худой конец... Но довольно обо мне. Что бы ты хотел узнать о работе в Creative Mobile, хозяин?",
                            "state":"/Hello"
                        },
                        {
                            "type":"buttons",
                            "buttons":[
                                {"text":"Хочу в отпуск", "transition":"/Vacation"},
                                {"text":"Я заболел", "transition":"/Sickness"},
                                {"text":"Newsroom", "transition":"/Newsroom"}
                            ],
                            "state":"/Hello"
                        }
                    ]
                },
                "timestamp":"2018-04-10T14:19:17.770"
            } */

            var response = new Response();

            foreach (Dictionary<string, object> reply in replies)
                if (reply["type"].ToString() == "text")
                {
                    response.output = reply["text"].ToString();
                    response.input = reply["state"].ToString().Trim(new char[] { '/', '.' });
                }
                else if (reply["type"].ToString() == "buttons")
                {
                    var buttons = reply["buttons"] as List<object>;
                    response.samples = new List<string>(buttons.Count);
                    foreach (Dictionary<string, object> button in buttons)
                        response.samples.Add(button["text"].ToString());
                }

            return response;
        }

        /// <summary> Response Text </summary>
        public string output;

        /// <summary> Response Id </summary>
        public string input;

        public string context;

        public string lang;

        public float score;

        public bool modal;

        /// <summary> Quick replies </summary>
        public List<string> samples;
        public List<Reply> replies
        {
            get
            {
                var _replies = new List<Reply>();
                foreach (var sample in samples)
                    if (sample.Contains("{")) _replies.Add(Reply.FromJson(sample));
                    else _replies.Add(new Reply(sample));
                return _replies;
            }
        }
        /// <summary> Meta Data </summary>
        public List<Var> vars;

        public string imageUrl
        {
            get
            {
                if (vars != null)
                    foreach (var v in vars)
                        if (v.name == "image") return v.value;
                return null;
            }
        }

        public string url
        {
            get
            {
                if (vars != null)
                    foreach (var v in vars)
                        if (v.name == "url") return v.value;
                return null;
            }
        }
    }

    public class Request
    {
        public static Request FromJson(string json) { return JsonUtility.FromJson<Request>(json); }
        public string ToJson()
        {
            // return JsonUtility.ToJson(this);

            return Json.Serialize(new Dictionary<string, object>() {
                { "clientId", user },
                { "query", text },
            });
        }

        /// <summary> Request Text </summary>
        public string text;

        /// <summary> User Id (optional) </summary>
        public string user;

        /// <summary> Meta Data (optional) </summary>
        public List<Var> vars;

        public Request(string text, string user = null)
        {
            this.text = text;
            this.user = user;
        }
    }
}