﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

namespace SG
{
    public class BuildSettings : Core
    {
        [Header("Debug")]
        public DebugLevel debugLevel = DebugLevel.Debug;
        public enum DebugLevel { None = 0, Error = 1, Release = 2, Debug = 3 }

        public Platform debugPlatform = Platform.Unknown;

        [Header("Features")]
        public bool ingameAdvisor = true;

        [Space(10)]
        public string version;
        public int versionCode;
        [HideInInspector]
        public int currentVersionCode;
        public bool isUpdateNeeded { get { return versionCode < currentVersionCode; } }
        [HideInInspector]
        public int criticalVersionCode;
        public bool isCriticalUpdateNeeded { get { return versionCode < criticalVersionCode; } }

        [Space(10)]
        public string keystorePass;
        public string keyaliasName;
        public string keyaliasPass;

        [Space(10)]
        public string productName;
        public string ID;
        public string APPLE_ID;
        public string appleDeveloperTeamID;

        [Space(10)]
        public AndroidStore androidStore = AndroidStore.GooglePlay;
        public enum AndroidStore { GooglePlay, Amazon }

        [Space(10)]
        public int maxFPS = 60;

        [Space(10)]
        [TextArea(5, 10)]
        public string notes;

        public void SetupDebug()
        {
            debugLevel = DebugLevel.Debug;

            Setup();
        }

        public void SetupRelease()
        {
            debugLevel = DebugLevel.Release;

            Setup();
        }

        public void Setup()
        {
            var defineSymbols = new System.Text.StringBuilder();
#if UNITY_EDITOR
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, defineSymbols.ToString());
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, defineSymbols.ToString());
            PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.WebGL, defineSymbols.ToString());

            PlayerSettings.productName = productName;
            PlayerSettings.applicationIdentifier = ID;
            PlayerSettings.bundleVersion = version;
#if UNITY_TIZEN
        PlayerSettings.bundleVersion += "." + versionCode;
#endif
            PlayerSettings.iOS.buildNumber = versionCode.ToString();
            PlayerSettings.Android.bundleVersionCode = versionCode;
            PlayerSettings.Android.keystorePass = keystorePass;
            PlayerSettings.Android.keyaliasName = keyaliasName;
            PlayerSettings.Android.keyaliasPass = keyaliasPass;
            PlayerSettings.iOS.appleDeveloperTeamID = appleDeveloperTeamID;
#endif

#if UNITY_EDITOR
            EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
            AssetDatabase.SaveAssets();
#endif
        }
    }
}