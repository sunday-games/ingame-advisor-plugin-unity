﻿using UnityEngine;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using System.Net;
using System.Net.Sockets;

namespace SG
{
    public class Server : Core
    {
        public float timeout = 10f;

        [Space(10)]
        public Links links;
        [Serializable]
        public class Links
        {
            public string checkConnection;          // https://www.google.com

            public string[] timeServers = new[] { "pool.ntp.org", "time1.google.com" };
        }

        public DateTime timeUTC;
        public void UpdateTime(Action<bool> callback)
        {
            foreach (var url in links.timeServers)
            {
                try
                {
                    var ntpData = new byte[48];
                    ntpData[0] = 0x1B; // LeapIndicator = 0 (no warning), VersionNum = 3 (IPv4 only), Mode = 3 (Client Mode)

                    var ipEndPoint = new IPEndPoint(Dns.GetHostEntry(url).AddressList[0], 123);
                    var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                    socket.SendTimeout = socket.ReceiveTimeout = 8000;
                    socket.Connect(ipEndPoint);
                    socket.Send(ntpData);
                    socket.Receive(ntpData);
                    socket.Close();

                    var intPart = (ulong)ntpData[40] << 24 | (ulong)ntpData[41] << 16 | (ulong)ntpData[42] << 8 | ntpData[43];
                    var fractPart = (ulong)ntpData[44] << 24 | (ulong)ntpData[45] << 16 | (ulong)ntpData[46] << 8 | ntpData[47];
                    var milliseconds = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);

                    timeUTC = new DateTime(1900, 1, 1).AddMilliseconds((long)milliseconds);
                    //lastUpdateTime = DateTime.Now;

                    Log("Server - UpdateTime - Success. Time Shift: {0}", (DateTime.UtcNow - timeUTC).TotalSeconds);
                    callback(true);
                    return;
                }
                catch (Exception e)
                {
                    LogError("Server - UpdateTime - {0} - Error: {1}", url, e);
                }
            }

            callback(false);
        }

        public void DownloadPic(UnityEngine.UI.RawImage image, string url)
        {
            Download.Create(gameObject).Run("Downloading Pic", url, download =>
            {
                if (download.isSuccess)
                {
                    var texture = download.www.texture;
                    if (texture != null) image.texture = texture;
                }
            });
        }

        public void DownloadPic(UnityEngine.UI.Image image, string url)
        {
            Download.Create(gameObject).Run("Downloading Pic", url, download =>
            {
                if (download.isSuccess)
                {
                    var texture = download.www.texture;
                    if (texture != null)
                        image.sprite = Sprite.Create(texture, new Rect(0f, 0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                }
            });
        }

        public void CheckConnection(Action<bool> callback)
        {
            if (platform == Platform.Facebook) { callback(true); return; }

            Download.Create(gameObject).Run("Check Connection", links.checkConnection,
                download => { callback(download.isSuccess); });
        }
    }
}