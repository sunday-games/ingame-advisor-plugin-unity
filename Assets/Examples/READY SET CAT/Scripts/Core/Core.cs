﻿using UnityEngine;

namespace SG
{
    public enum Platform { Unknown, Editor, AppStore, GooglePlay, Facebook, Amazon, WindowsPhone, Tizen, tvOS }

    public abstract class Core : MonoBehaviour
    {
        static BuildSettings _build;
        public static BuildSettings build { get { if (_build == null) _build = FindObjectOfType(typeof(BuildSettings)) as BuildSettings; return _build; } }

        public static Platform platform = Platform.Unknown;
        public static Server server;
        public static IngameAdvisor.Advisor advisor;
        public static AdvisorUI advisorUI;

        public static void LogError(object text)
        {
            if ((int)build.debugLevel > 0) Debug.LogError(text);
        }
        public static void LogError(object text, params object[] parameters)
        {
            if ((int)build.debugLevel > 0) Debug.LogError(string.Format(text.ToString(), parameters));
        }
        public static void Log(object text)
        {
            if ((int)build.debugLevel > 1) Debug.Log(text);
        }
        public static void Log(object text, params object[] parameters)
        {
            if ((int)build.debugLevel > 1) Debug.Log(string.Format(text.ToString(), parameters));
        }
        public static void LogDebug(object text)
        {
            if ((int)build.debugLevel > 2) Debug.Log(text);
        }
        public static void LogDebug(object text, params object[] parameters)
        {
            if ((int)build.debugLevel > 2) Debug.Log(string.Format(text.ToString(), parameters));
        }
    }
}