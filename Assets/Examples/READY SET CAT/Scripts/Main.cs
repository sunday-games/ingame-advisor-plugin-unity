﻿using UnityEngine;

namespace SG
{
    public class Main : Core
    {
        void Awake()
        {
#if UNITY_EDITOR
            platform = Platform.Editor;
#elif UNITY_WEBGL
            platform = Platform.Facebook;
#elif UNITY_TIZEN
	        platform = Platform.Tizen;
#elif UNITY_IOS
	        platform = Platform.AppStore;
#elif UNITY_ANDROID
            if (build.androidStore == BuildSettings.AndroidStore.GooglePlay) platform = Platform.GooglePlay;
            else platform = Platform.Amazon;
#endif
            if (build.debugPlatform != Platform.Unknown) platform = build.debugPlatform;

            if (build.maxFPS > 0) Application.targetFrameRate = build.maxFPS;

            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            Localization.Init();
            // Localization.SetLanguage(Application.systemLanguage);
            Localization.SetLanguage(SystemLanguage.English);

            server = FindObjectOfType(typeof(Server)) as Server;
            advisorUI = FindObjectOfType(typeof(AdvisorUI)) as AdvisorUI;
            advisor = IngameAdvisor.Advisor.instance;

            // advisor.projects = new List<IngameAdvisor.Advisor.Project>() { new IngameAdvisor.Advisor.Project(SystemLanguage.English, "projectId") };
        }
    }
}