﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using DG.Tweening;
using IngameAdvisor;

namespace SG
{
    public class AdvisorUI : Core
    {
        public string startText;

        [Space(10)]
        public Dropdown languageDropdown;
        List<string> languages = new List<string>();
        public SystemLanguage language
        {
            get
            {
                return languages[languageDropdown.value].ToEnum<SystemLanguage>();
            }
            set
            {
                var i = languages.IndexOf(value.ToString());
                languageDropdown.value = i < 0 ? 0 : i;
            }
        }

        [Space(10)]
        public Animator catAnimator;
        public AudioSource catAudioSource;

        [Space(10)]
        public Bubble replyBubble;
        public Vector2 replyBubbleHeight = new Vector2(210, 730f);

        [Space(10)]
        public Bubble askBubble;

        [Space(10)]
        public InputField askInputField;
        public GameObject micImage;
        public GameObject keyboardImage;

        [Space(10)]
        public AskButton askButtonPrefab;
        List<AskButton> askButtons = new List<AskButton>();
        public GridLayoutGroup askButtonsGrid;
        public Vector2 askButtonsPositionKeyboardNone;
        public Vector2 askButtonsPositionKeyboardIOS;
        public Vector2 askButtonsPositionKeyboardAndroid;
        Vector2 askButtonsPositionKeyboard;

        [Space(10)]
        public GameObject testsWindow;
        public Text testsText;
        public Text perfTestsText;

        // How many times in a row the adviser isn't understood the user
        int misunderstandingsCount = 0;

        void Start()
        {
            askButtonsRectTransform = askButtonsGrid.transform as RectTransform;

            foreach (var project in advisor.projects)
                languages.Add(project.language.ToString());

            if (languages.Count == 0)
            {
                LogError("There isnt any language");
                return;
            }

            languageDropdown.ClearOptions();
            languageDropdown.AddOptions(languages);

            language = Localization.language;

            if (platform == Platform.AppStore)
                askButtonsPositionKeyboard = askButtonsPositionKeyboardIOS;
            else if (platform == Platform.GooglePlay)
                askButtonsPositionKeyboard = askButtonsPositionKeyboardAndroid;
            else
                askButtonsPositionKeyboard = askButtonsPositionKeyboardNone;

            Reset();

            Ask(startText);
            //ReplyBubbleShow(Localization.Get("advisorReplyDefault"));

            // Analytic.Event("Advisor", "Open");
        }

        public void OnAdvisorClick()
        {
            catAnimator.SetTrigger("Tap");
            catAudioSource.pitch = Random.Range(0.8f, 1.2f);
            catAudioSource.Play();
        }

        public void OnChangeLanguage()
        {
            Localization.SetLanguage(language);
        }

        RectTransform askButtonsRectTransform;
        bool lastState = false;
        public void Update()
        {
            micImage.SetActive(string.IsNullOrEmpty(askInputField.text));
            keyboardImage.SetActive(string.IsNullOrEmpty(askInputField.text));

            if (lastState != askInputField.isFocused)
            {
                lastState = askInputField.isFocused;
                askButtonsGrid.DOKill();

                Vector2 to = askButtonsPositionKeyboardNone;

                if (askInputField.isFocused && (platform == Platform.AppStore || platform == Platform.GooglePlay))
                    to = askButtonsPositionKeyboard
                        - (askButtonsRectTransform.childCount - 2) * new Vector2(0f, askButtonsGrid.cellSize.y + askButtonsGrid.spacing.y);

                askButtonsRectTransform.DOAnchorPos(to, 0.4f);
            }
        }

        public void Reset(string ask = null)
        {
            foreach (var replyButton in askButtons) Destroy(replyButton.gameObject);
            askButtons.Clear();

            replyBubble.Hide();

            askBubble.Hide();
            if (!string.IsNullOrEmpty(ask)) askBubble.Show(ask, replyBubbleHeight);

            if (platform != Platform.AppStore && platform != Platform.GooglePlay)
            {
                askInputField.Select();
                askInputField.ActivateInputField();
            }
        }

        public void AskFromInputField()
        {
            if (string.IsNullOrEmpty(askInputField.text) || !advisor) return;

            Ask(askInputField.text);
            askInputField.text = string.Empty;
        }

        public void Ask(string text)
        {
            Reset(text);

            var request = new Request(text, SystemInfo.deviceUniqueIdentifier);
            request.vars = new List<Var>() {
                new Var("platform", platform),
                new Var("misunderstandingsCount", misunderstandingsCount),
                new Var("facebookLogin", false), // new Advisor.Var("facebookLogin", fb.isLogin),
                new Var("musicOn", true), //new Advisor.Var("musicOn", music.ON),
            };

            advisor.Ask(request, language, OnGetReply);
        }

        public void OnGetReply(Status status, Response response, Request request, Test test)
        {
            if (status == Status.NoConnection)
            {
                ReplyBubbleShow(Localization.Get("advisorReplyNoConnection"));
                return;
            }

            if (status != Status.Success || response == null)
            {
                ReplyBubbleShow(Localization.Get("advisorReplyError"));
                return;
            }

            // Analytic.EventProperties("Advisor", response.input, request.text);

            ReplyBubbleShow(response.output);

            if (!string.IsNullOrEmpty(response.imageUrl))
                server.DownloadPic(replyBubble.bubbleContentImage, response.imageUrl);

            if (!string.IsNullOrEmpty(response.url))
                Utils.OpenLink(response.url, response.output);

            var replies = response.replies;
            while (replies.Count > 3)
                replies.Remove(replies[Random.Range(0, replies.Count)]);
            foreach (var reply in replies)
                askButtons.Add(askButtonPrefab.Copy(reply));

            misunderstandingsCount = response.input == "All" ? misunderstandingsCount + 1 : 0;
        }

        public void ReplyBubbleShow(string text)
        {
            replyBubble.Show(text,
                replyBubbleHeight - (askButtonsRectTransform.childCount - 2) * new Vector2(0f, askButtonsGrid.cellSize.y + askButtonsGrid.spacing.y));
        }

        public void OnAskButtonClick(Reply reply)
        {
            if (!string.IsNullOrEmpty(reply.url))
            {
                Utils.OpenLink(reply.url, reply.name);

                Reset(reply.name);
                if (!string.IsNullOrEmpty(reply.answer)) ReplyBubbleShow(reply.answer);
            }
            else if (!string.IsNullOrEmpty(reply.email))
            {
                Utils.Email(reply.email, reply.subject, reply.body);

                Reset(reply.name);
                if (!string.IsNullOrEmpty(reply.answer)) ReplyBubbleShow(reply.answer);
            }
            else
            {
                Ask(reply.name);
            }
        }

        public void RunTests()
        {
            foreach (var project in advisor.projects)
                if (project.language == language)
                {
                    testsText.text = null;
                    testsWindow.SetActive(true);

                    Tests.instance.Run(project, testsText);

                    break;
                }
        }

        public void RunPerf()
        {
            foreach (var project in advisor.projects)
                if (project.language == language)
                {
                    Tests.instance.RunPerformance(project, perfTestsText);

                    break;
                }
        }
    }
}