﻿using UnityEngine.UI;
using IngameAdvisor;

namespace SG
{
    public class AskButton : Core
    {
        public Text mainText;
        Reply reply;

        public AskButton Copy(Reply reply)
        {
            var instance = Instantiate(this);
            instance.transform.SetParent(this.transform.parent, false);
            instance.Setup(reply);
            return instance;
        }

        public void Setup(Reply reply)
        {
            this.reply = reply;

            mainText.text = reply.name;

            gameObject.SetActive(true);
        }

        public void OnClick()
        {
            advisorUI.OnAskButtonClick(reply);
        }
    }
}